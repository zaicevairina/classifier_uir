#для получения данных из БД
import pandas as pd
from google.oauth2 import service_account
project_id = 'arctic-task-238719'
private_key='arctic-task-238719-e6a1c5fe056b.json'
from google.cloud import bigquery
credentials = service_account.Credentials.from_service_account_file('C:/Users/Ирина/PycharmProjects/classifier/venv/arctic-task-238719-e6a1c5fe056b.json')
from pandas.io import gbq

#для работы классификатора и построения графиков
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.pipeline import Pipeline
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle
from sklearn import  svm,datasets
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier
from scipy import interp
from sklearn.metrics import roc_auc_score
from sklearn.metrics import f1_score


sum_precision_macro = 0
sum_precision_micro = 0
sum_recall_macro = 0
sum_recall_micro = 0
sum_f_macro = 0
sum_f_micro = 0
sum_roc_auc = 0



#получение выборки из БД и запись в массив
sQuery = '''
    SELECT *
    FROM [learning.sample]
'''

df = gbq.read_gbq(sQuery, project_id,credentials=credentials)
count_ob=len(df['text'])
df['text'] = df['text'].astype(str)

X=[]      #набор текстов
for i in range(count_ob):
     X.append(0)
y=[]       #набор меток
for i in range(count_ob):
     y.append(0)

for i in range(count_ob) :
    X[i]=df['text'][i]
    y[i]=df['label'][i]

#бинаризация
y = label_binarize(y, classes=[1,2,3,4,5,6])
n_classes = y.shape[1]

#определение выборки
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.25, random_state=5)
target_names = ['1', '2', '3', '4', '5', '6']





# Алгоритм RandomForestClassifier(n_estimators=100)
classifier1 = Pipeline([
    ('tfidf', TfidfVectorizer(lowercase=False)),
    ('clf', OneVsRestClassifier(RandomForestClassifier(n_estimators=10,criterion='gini')))])

classifier2 = Pipeline([
    ('tfidf', TfidfVectorizer(lowercase=False)),
    ('clf', OneVsRestClassifier(RandomForestClassifier(n_estimators=50,criterion='gini')))])

classifier3 = Pipeline([
    ('tfidf', TfidfVectorizer(lowercase=False)),
    ('clf', OneVsRestClassifier(RandomForestClassifier(n_estimators=100,criterion='gini')))])

classifier4 = Pipeline([
    ('tfidf', TfidfVectorizer(lowercase=False)),
    ('clf', OneVsRestClassifier(RandomForestClassifier(n_estimators=10,criterion='entropy')))])

classifier5 = Pipeline([
    ('tfidf', TfidfVectorizer(lowercase=False)),
    ('clf', OneVsRestClassifier(RandomForestClassifier(n_estimators=50,criterion='entropy')))])

classifier6 = Pipeline([
    ('tfidf', TfidfVectorizer(lowercase=False)),
    ('clf', OneVsRestClassifier(RandomForestClassifier(n_estimators=100,criterion='entropy')))])

classifier7 = Pipeline([
    ('tfidf', TfidfVectorizer(lowercase=False,binary='True')),
    ('clf', OneVsRestClassifier(svm.SVC(kernel='linear', probability=True, C=1)))])

classifier8 = Pipeline([
    ('tfidf', TfidfVectorizer(lowercase=False,binary='True')),
    ('clf', OneVsRestClassifier(svm.SVC(kernel='linear', probability=True, C=5)))])

classifier9 = Pipeline([
    ('tfidf', TfidfVectorizer(lowercase=False,binary='True')),
    ('clf', OneVsRestClassifier(svm.SVC(kernel='linear', probability=True, C=10)))])

classifier10 = Pipeline([
    ('tfidf', TfidfVectorizer(lowercase=False,binary='True')),
    ('clf', OneVsRestClassifier(svm.SVC(kernel='poly', probability=True, C=1,gamma='scale',coef0=10)))])

classifier11 = Pipeline([
    ('tfidf', TfidfVectorizer(lowercase=False,binary='True')),
    ('clf', OneVsRestClassifier(svm.SVC(kernel='poly', probability=True, C=5,gamma='scale',coef0=10)))])

classifier12 = Pipeline([
    ('tfidf', TfidfVectorizer(lowercase=False,binary='True')),
    ('clf', OneVsRestClassifier(svm.SVC(kernel='poly', probability=True, C=10,gamma='scale',coef0=10)))])

k=10
for i in range(k):
    print("СЛУЧАЙНАЯ ВЫБОРКА №", i + 1)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.25, random_state=i)

    classifier1.fit(X_train, y_train)
    y_score1 = classifier1.predict_proba(X_test)
    y_pred1 = classifier1.predict(X_test)

    classifier2.fit(X_train, y_train)
    y_score2 = classifier2.predict_proba(X_test)
    y_pred2 = classifier2.predict(X_test)

    classifier3.fit(X_train, y_train)
    y_score3 = classifier3.predict_proba(X_test)
    y_pred3 = classifier3.predict(X_test)

    classifier4.fit(X_train, y_train)
    y_score4 = classifier4.predict_proba(X_test)
    y_pred4 = classifier4.predict(X_test)

    classifier5.fit(X_train, y_train)
    y_score5 = classifier5.predict_proba(X_test)
    y_pred5 = classifier5.predict(X_test)

    classifier6.fit(X_train, y_train)
    y_score6 = classifier6.predict_proba(X_test)
    y_pred6 = classifier6.predict(X_test)
    #
    classifier7.fit(X_train, y_train)
    y_score7 = classifier7.decision_function(X_test)
    y_pred7 = classifier7.predict(X_test)

    classifier8.fit(X_train, y_train)
    y_score8 = classifier8.decision_function(X_test)
    y_pred8 = classifier8.predict(X_test)

    classifier9.fit(X_train, y_train)
    y_score9 = classifier9.decision_function(X_test)
    y_pred9 = classifier9.predict(X_test)

    classifier10.fit(X_train, y_train)
    y_score10 = classifier10.decision_function(X_test)
    y_pred10 = classifier10.predict(X_test)

    classifier11.fit(X_train, y_train)
    y_score11 = classifier11.decision_function(X_test)
    y_pred11 = classifier11.predict(X_test)

    classifier12.fit(X_train, y_train)
    y_score12 = classifier12.decision_function(X_test)
    y_pred12 = classifier12.predict(X_test)

    # print(classification_report(y_test, y_pred1, target_names=target_names))
    # print(classification_report(y_test, y_pred2, target_names=target_names))
    # print(classification_report(y_test, y_pred3, target_names=target_names))
    # print(classification_report(y_test, y_pred4, target_names=target_names))
    # print(classification_report(y_test, y_pred5, target_names=target_names))
    # print(classification_report(y_test, y_pred6, target_names=target_names))
    # print(classification_report(y_test, y_pred7, target_names=target_names))
    # print(classification_report(y_test, y_pred8, target_names=target_names))
    # print(classification_report(y_test, y_pred9, target_names=target_names))
    # print(classification_report(y_test, y_pred10, target_names=target_names))
    # print(classification_report(y_test, y_pred11, target_names=target_names))
    # print(classification_report(y_test, y_pred12, target_names=target_names))




    if (True):
        # Построение ROC-кривой для каждого касса
        fpr1 = dict()
        tpr1 = dict()
        roc_auc1 = dict()
        fpr2 = dict()
        tpr2 = dict()
        roc_auc2 = dict()
        fpr3 = dict()
        tpr3 = dict()
        roc_auc3 = dict()
        fpr4 = dict()
        tpr4 = dict()
        roc_auc4 = dict()
        fpr5 = dict()
        tpr5 = dict()
        roc_auc5 = dict()
        fpr6 = dict()
        tpr6 = dict()
        roc_auc6 = dict()
        fpr7 = dict()
        tpr7 = dict()
        roc_auc7 = dict()
        fpr8 = dict()
        tpr8 = dict()
        roc_auc8 = dict()
        fpr9 = dict()
        tpr9 = dict()
        roc_auc9 = dict()
        fpr10 = dict()
        tpr10 = dict()
        roc_auc10 = dict()
        fpr11 = dict()
        tpr11 = dict()
        roc_auc11 = dict()
        fpr12 = dict()
        tpr12 = dict()
        roc_auc12 = dict()


        for i in range(n_classes):
            fpr1[i], tpr1[i], _ = roc_curve(y_test[:, i], y_score1[:, i])
            roc_auc1[i] = auc(fpr1[i], tpr1[i])
        for i in range(n_classes):
            fpr2[i], tpr2[i], _ = roc_curve(y_test[:, i], y_score2[:, i])
            roc_auc2[i] = auc(fpr2[i], tpr2[i])
        for i in range(n_classes):
            fpr3[i], tpr3[i], _ = roc_curve(y_test[:, i], y_score3[:, i])
            roc_auc3[i] = auc(fpr3[i], tpr3[i])
        for i in range(n_classes):
            fpr4[i], tpr4[i], _ = roc_curve(y_test[:, i], y_score4[:, i])
            roc_auc4[i] = auc(fpr4[i], tpr4[i])
        for i in range(n_classes):
            fpr5[i], tpr5[i], _ = roc_curve(y_test[:, i], y_score5[:, i])
            roc_auc5[i] = auc(fpr5[i], tpr5[i])
        for i in range(n_classes):
            fpr6[i], tpr6[i], _ = roc_curve(y_test[:, i], y_score6[:, i])
            roc_auc6[i] = auc(fpr6[i], tpr6[i])
        for i in range(n_classes):
            fpr7[i], tpr7[i], _ = roc_curve(y_test[:, i], y_score7[:, i])
            roc_auc7[i] = auc(fpr7[i], tpr7[i])
        for i in range(n_classes):
            fpr8[i], tpr8[i], _ = roc_curve(y_test[:, i], y_score8[:, i])
            roc_auc8[i] = auc(fpr8[i], tpr8[i])
        for i in range(n_classes):
            fpr9[i], tpr9[i], _ = roc_curve(y_test[:, i], y_score9[:, i])
            roc_auc9[i] = auc(fpr9[i], tpr9[i])
        for i in range(n_classes):
            fpr10[i], tpr10[i], _ = roc_curve(y_test[:, i], y_score10[:, i])
            roc_auc10[i] = auc(fpr10[i], tpr10[i])
        for i in range(n_classes):
            fpr11[i], tpr11[i], _ = roc_curve(y_test[:, i], y_score11[:, i])
            roc_auc11[i] = auc(fpr11[i], tpr11[i])
        for i in range(n_classes):
            fpr12[i], tpr12[i], _ = roc_curve(y_test[:, i], y_score12[:, i])
            roc_auc12[i] = auc(fpr12[i], tpr12[i])





        all_fpr = np.unique(np.concatenate([fpr1[i] for i in range(n_classes)]))
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr1[i], tpr1[i])
        mean_tpr /= n_classes
        fpr1["macro"] = all_fpr
        tpr1["macro"] = mean_tpr
        roc_auc1["macro"] = auc(fpr1["macro"], tpr1["macro"])
        plt.figure()
        plt.plot(fpr1["macro"], tpr1["macro"],
                 label='Классификатор 1 (area = {0:0.2f})'
                       ''.format(roc_auc1["macro"]),
                 color='navy', linewidth=1)

        all_fpr = np.unique(np.concatenate([fpr2[i] for i in range(n_classes)]))
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr2[i], tpr2[i])
        mean_tpr /= n_classes
        fpr2["macro"] = all_fpr
        tpr2["macro"] = mean_tpr
        roc_auc2["macro"] = auc(fpr2["macro"], tpr2["macro"])

        plt.plot(fpr2["macro"], tpr2["macro"],
                 label='Классификатор 2 (area = {0:0.2f})'
                       ''.format(roc_auc2["macro"]),
                 color='green', linewidth=1)



        all_fpr = np.unique(np.concatenate([fpr3[i] for i in range(n_classes)]))
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr3[i], tpr3[i])
        mean_tpr /= n_classes
        fpr3["macro"] = all_fpr
        tpr3["macro"] = mean_tpr
        roc_auc3["macro"] = auc(fpr3["macro"], tpr3["macro"])

        plt.plot(fpr3["macro"], tpr3["macro"],
                 label='Классификатор 3 (area = {0:0.2f})'
                       ''.format(roc_auc3["macro"]),
                 color='red', linewidth=1)

        all_fpr = np.unique(np.concatenate([fpr4[i] for i in range(n_classes)]))
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr4[i], tpr4[i])
        mean_tpr /= n_classes
        fpr4["macro"] = all_fpr
        tpr4["macro"] = mean_tpr
        roc_auc4["macro"] = auc(fpr4["macro"], tpr4["macro"])
        plt.plot(fpr4["macro"], tpr4["macro"],
                 label='Классификатор 4 (area = {0:0.2f})'
                       ''.format(roc_auc4["macro"]),
                 color='blue', linewidth=1)
        all_fpr = np.unique(np.concatenate([fpr5[i] for i in range(n_classes)]))
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr5[i], tpr5[i])
        mean_tpr /= n_classes
        fpr5["macro"] = all_fpr
        tpr5["macro"] = mean_tpr
        roc_auc5["macro"] = auc(fpr5["macro"], tpr5["macro"])
        plt.plot(fpr5["macro"], tpr5["macro"],
                 label='Классификатор 5 (area = {0:0.2f})'
                       ''.format(roc_auc5["macro"]),
                 color='black', linewidth=1)
        all_fpr = np.unique(np.concatenate([fpr6[i] for i in range(n_classes)]))
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr6[i], tpr6[i])
        mean_tpr /= n_classes
        fpr6["macro"] = all_fpr
        tpr6["macro"] = mean_tpr
        roc_auc6["macro"] = auc(fpr6["macro"], tpr6["macro"])
        plt.plot(fpr6["macro"], tpr6["macro"],
                 label='Классификатор 6 (area = {0:0.2f})'
                       ''.format(roc_auc6["macro"]),
                 color='yellow', linewidth=1)
        all_fpr = np.unique(np.concatenate([fpr7[i] for i in range(n_classes)]))
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr7[i], tpr7[i])
        mean_tpr /= n_classes
        fpr7["macro"] = all_fpr
        tpr7["macro"] = mean_tpr
        roc_auc7["macro"] = auc(fpr7["macro"], tpr7["macro"])
        plt.plot(fpr7["macro"], tpr7["macro"],
                 label='Классификатор 7 (area = {0:0.2f})'
                       ''.format(roc_auc7["macro"]),
                 color='grey', linewidth=1)
        all_fpr = np.unique(np.concatenate([fpr8[i] for i in range(n_classes)]))
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr8[i], tpr8[i])
        mean_tpr /= n_classes
        fpr8["macro"] = all_fpr
        tpr8["macro"] = mean_tpr
        roc_auc8["macro"] = auc(fpr8["macro"], tpr8["macro"])
        plt.plot(fpr8["macro"], tpr8["macro"],
                 label='Классификатор 8 (area = {0:0.2f})'
                       ''.format(roc_auc8["macro"]),
                 color='deeppink', linewidth=1)
        all_fpr = np.unique(np.concatenate([fpr9[i] for i in range(n_classes)]))
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr9[i], tpr9[i])
        mean_tpr /= n_classes
        fpr9["macro"] = all_fpr
        tpr9["macro"] = mean_tpr
        roc_auc9["macro"] = auc(fpr9["macro"], tpr9["macro"])
        plt.plot(fpr9["macro"], tpr9["macro"],
                 label='Классификатор 9 (area = {0:0.2f})'
                       ''.format(roc_auc9["macro"]),
                 color='pink', linewidth=1)

        all_fpr = np.unique(np.concatenate([fpr10[i] for i in range(n_classes)]))
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr10[i], tpr10[i])
        mean_tpr /= n_classes
        fpr10["macro"] = all_fpr
        tpr10["macro"] = mean_tpr
        roc_auc10["macro"] = auc(fpr10["macro"], tpr10["macro"])
        plt.plot(fpr10["macro"], tpr10["macro"],
                 label='Классификатор 10 (area = {0:0.2f})'
                       ''.format(roc_auc10["macro"]),
                 color='maroon', linewidth=1)

        all_fpr = np.unique(np.concatenate([fpr11[i] for i in range(n_classes)]))
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr11[i], tpr11[i])
        mean_tpr /= n_classes
        fpr11["macro"] = all_fpr
        tpr11["macro"] = mean_tpr
        roc_auc11["macro"] = auc(fpr11["macro"], tpr11["macro"])
        plt.plot(fpr11["macro"], tpr11["macro"],
                 label='Классификатор 11 (area = {0:0.2f})'
                       ''.format(roc_auc11["macro"]),
                 color='lime', linewidth=1)

        all_fpr = np.unique(np.concatenate([fpr12[i] for i in range(n_classes)]))
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr12[i], tpr12[i])
        mean_tpr /= n_classes
        fpr12["macro"] = all_fpr
        tpr12["macro"] = mean_tpr
        roc_auc12["macro"] = auc(fpr12["macro"], tpr12["macro"])
        plt.plot(fpr12["macro"], tpr12["macro"],
                 label='Классификатор 12 (area = {0:0.2f})'
                       ''.format(roc_auc12["macro"]),
                 color='tan', linewidth=1)

        #

        plt.plot([0, 1], [0, 1], 'k--', lw=2)
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('macro-average ROC-кривые'
                  ' для многоклассовых классификаторов с различными параметрами')
        plt.legend(loc="lower right")
        plt.show()

    # sum_precision_macro = sum_precision_macro + precision_score(y_test, y_pred12, average='macro')
    # sum_precision_micro = sum_precision_micro + precision_score(y_test, y_pred12, average='micro')
    # sum_recall_macro = sum_recall_macro + recall_score(y_test, y_pred12, average='macro')
    # sum_recall_micro = sum_recall_micro + recall_score(y_test, y_pred12, average='micro')
    # sum_f_macro = sum_f_macro + f1_score(y_test, y_pred12, average='macro')
    # sum_f_micro = sum_f_micro + f1_score(y_test, y_pred12, average='micro')
    # sum_roc_auc = sum_roc_auc + roc_auc_score(y_test, y_pred12, average='macro')

# print("precision_macro")
# print(sum_precision_macro/k)
# print("precision_micro")
# print(sum_precision_micro/k)
# print("recall_micro")
# print(sum_recall_macro/k)
# print("recall_macro")
# print(sum_recall_micro/k)
# print("f_macro")
# print(sum_f_macro/k)
# print("f_micro")
# print(sum_f_micro/k)
# print("auc")
# print(sum_roc_auc/k)

